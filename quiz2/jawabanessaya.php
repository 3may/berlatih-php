<?php
function hitung($string_data)
{
    $kali = strpos($string_data, "*");
    $tambah = strpos($string_data, "+");
    $bagi = strpos($string_data, ":");
    $mod = strpos($string_data, "%");
    $kurang = strpos($string_data, "-");

    if ($kali) {
        $angkakali2 = substr($string_data, strpos($string_data, "*") + 1);
        $pertama = explode("*", $string_data, 2);
        $angkakali1 = $pertama[0];

        echo intval($angkakali1) * intval($angkakali2);
        echo "<br>";
    } elseif ($tambah) {
        $angkakali2 = substr($string_data, strpos($string_data, "+") + 1);
        $pertama = explode("+", $string_data, 2);
        $angkakali1 = $pertama[0];

        echo intval($angkakali1) + intval($angkakali2);
        echo "<br>";
    } elseif ($bagi) {
        $angkakali2 = substr($string_data, strpos($string_data, ":") + 1);
        $pertama = explode(":", $string_data, 2);
        $angkakali1 = $pertama[0];

        echo intval($angkakali1) / intval($angkakali2);
        echo "<br>";
    } elseif ($mod) {
        $angkakali2 = substr($string_data, strpos($string_data, "%") + 1);
        $pertama = explode("%", $string_data, 2);
        $angkakali1 = $pertama[0];

        echo intval($angkakali1) % intval($angkakali2);
        echo "<br>";
    } else {
        $angkakali2 = substr($string_data, strpos($string_data, "-") + 1);
        $pertama = explode("-", $string_data, 2);
        $angkakali1 = $pertama[0];

        echo intval($angkakali1) - intval($angkakali2);
        echo "<br>";
    }
}

echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");
