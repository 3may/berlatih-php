<?php
function ubah_huruf($string)
{
    //kode di sini
    $n = 1;
    static $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $n = (int)$n % 26;
    if (!$n) return $string;
    for ($i = 0, $l = strlen($string); $i < $l; $i++) {
        $c = $string[$i];
        if ($c >= 'a' && $c <= 'z') {
            $string[$i] = $letters[(ord($c) - 71 + $n) % 26];
        } else if ($c >= 'A' && $c <= 'Z') {
            $string[$i] = $letters[(ord($c) - 39 + $n) % 26 + 26];
        }
    }
    echo "<br>";
    return $string;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
