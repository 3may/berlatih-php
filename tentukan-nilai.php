<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number >= 85 and $number <= 100) {
        echo "Sangat Baik<br>";
    } elseif ($number >= 70 and $number < 85) {
        echo "Baik<br>";
    } elseif ($number >= 60 and $number < 70) {
        echo "Cukup<br>";
    } else {
        echo "Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
