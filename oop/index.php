<?php
require_once 'animal.php';

echo "<b>Release 0 </b><br><br>";

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false
echo "<br>";
echo "<br>";
echo "<br>";


echo "<b>Release 1 </b><br><br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
