<?php
class Animal
{
    public $legs = 2;
    public $cold_blooded = 'false';
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}

class Frog extends Animal
{
    public function jump()
    {
        echo "hop hop<br>";
    }
}

class Ape extends Animal
{
    public function yell()
    {
        echo "Auooo<br>";
    }
}
